interface Beverage {

    var desription: String

    fun getDescription(): String = "Unknown beverage"

    fun cost(): Double
}


interface CondimentDecorator : Beverage {
    override fun getDescription(): String
}

class Espresso : Beverage {
    override var desription: String
        get() = "Espresso"
        set(value) {}

    override fun cost(): Double {
        return 1.99
    }


}

class HouseBlend : Beverage {
    override var desription: String
        get() = "HouseBlend"
        set(value) {}

    override fun cost(): Double {
        return 0.89
    }

}

class DarkRoast : Beverage {
    override fun getDescription(): String {
        return desription
    }

    override var desription: String
        get() = "DarkRoast"
        set(value) {}

    override fun cost(): Double {
        return 0.69
    }

}

class CocoaPowder(val beverage: Beverage):CondimentDecorator,Beverage by beverage
{
    override fun cost(): Double {
        return beverage.cost() +  0.20
    }

    override fun getDescription(): String {
        return beverage.getDescription()+" and 1 serving of Cocoa Powder"
    }

}

class Whip(val beverage: Beverage):CondimentDecorator,Beverage by beverage
{
    override fun cost(): Double {
        return beverage.cost() +  0.10
    }

    override fun getDescription(): String {
        return beverage.getDescription()+" and serving of whipped cream"
    }

}

fun main(args: Array<String>) {
    println("Starbuzzz Coffee")
    var dr:Beverage = DarkRoast()
    dr= CocoaPowder(dr)
    dr= CocoaPowder(dr)
    dr=Whip(dr)
    println("""The cost of the beverage with ingredients:
        |${dr.getDescription()} has cost of: $${dr.cost()}""".trimMargin())
}