import java.util.*

class Translate(val name: String)
 
class Cristina : Observable() {
 
    val name = "Cristina"
 
    fun translateEnglish(name : String){
        var translate = Translate(name)
 
        setChanged()
        notifyObservers(translate)
    }
}
 
class Tina : Observer{
 
    val name = "Sebastian"
 
    override fun update(o: Observable?, arg: Any?) {
        when (o){
            is Cristina -> {
                if (arg is Translate){
                    println("$name is proofreading the ${arg.name} translated by ${o.name}")
                }
            }
            else -> println(o?.javaClass.toString())
        }
    }
}
 
fun main(args : Array<String>){
    val cristina = Cristina()
    cristina.addObserver(Tina())
 
    cristina.translateEnglish("certificate")
}